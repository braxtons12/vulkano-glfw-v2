## vulkano-glfw-v2

Unfortune successor to vulkano-glfw, as the original author seems to no longer be interested in maintaining the library.

A small collection of utility functions to use GLFW together with the vulkano library.

[![docs.rs version](https://docs.rs/vulkano-glfw-v2/badge.svg)](https://docs.rs/vulkano-glfw-v2/)
[![crates.io version](https://img.shields.io/crates/v/vulkano-glfw-v2.svg)](https://crates.io/crates/vulkano-glfw-v2)
